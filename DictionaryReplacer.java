import java.util.HashMap;
import java.util.Map;

public class DictionaryReplacer {

    public static void main(String[] args ){

        Map<String, String> map = new HashMap<>();
        map.put("\\$temp\\$", "temporary");
        map.put("\\$name\\$", "John Doe");

        System.out.println(dict(map));
    }

    public static String dict (Map<String, String> map ){

        String chaineInput="";
        if ( map==null || map.size()==0){
            return chaineInput;
        } else  {
            chaineInput ="\\$temp\\$ here comes the name \\$name\\$";

            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (chaineInput.contains(entry.getKey())){
                    chaineInput = chaineInput.replace(entry.getKey(),entry.getValue());
                }
            }

            return chaineInput;
        }
    }

}
